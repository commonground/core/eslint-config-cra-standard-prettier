
module.exports = {
  parser: '@typescript-eslint/parser',

  extends: [
    'react-app',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'plugin:prettier/recommended',
    'plugin:security/recommended',
    '@commonground/eslint-config/rules/generic',
    '@commonground/eslint-config/rules/prettier',
    '@commonground/eslint-config/rules/header',
    '@commonground/eslint-config/rules/react',
    '@commonground/eslint-config/rules/import',
    '@commonground/eslint-config/rules/jest',
    '@commonground/eslint-config/rules/security',
  ],

  plugins: [
    'header',
    'jest',
  ],

  parserOptions: {
    ecmaFeatures: {
      jsx: true
    }
  },

  overrides: [
    {
      // Turn off explicit module boundary types for .js(x) files, so only .ts(x) are required to do so
      files: ['*.js', '*.jsx'],
      rules: {
        '@typescript-eslint/explicit-module-boundary-types': 0,
      }
    }
  ],

  rules: {
    'no-use-before-define': 0,
  },
}
