# eslint-config-cra-standard-prettier

Common Ground shared ESLint config for all our React projects. Designed to be used with create-react-app.

> Not using Create React App? See [our the Common Ground base ESLint rules](https://gitlab.com/commonground/core/eslint-config)

## Usage

Note: if you use a NPM version <7.x you should install the peer dependencies manually.

```shell
npm install --save-dev --save-exact \
@commonground/eslint-config-cra-standard-prettier
```

Then override the `eslintConfig` setting in `package.json`:

```json
"eslintConfig": {
  "extends": "@commonground/eslint-config-cra-standard-prettier"
}
```

Alternatively, you can remove the `eslintConfig` entry and create a `.eslintrc.js` file, with:

```js
module.exports = {
  extends: ['@commonground/eslint-config-cra-standard-prettier']
}
```

This last method also opens the possibility to extend the rules, but in order to maintain a generic developer experience in all our projects, we recommend against this.

## Editor integration

### VSCode

Add the following extension:

Then create the following file in the frontend project directory: `.vscode/settings.json` (it's .gitignored) containing:

```json
{
  "editor.defaultFormatter": "dbaeumer.vscode-eslint",
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  }
}
```

You may define this file at a higher level as well, but then make sure you add this to the settings.json:

```json
{
  "eslint.workingDirectories": [
    "./ui-directory"
  ]
}
```

## How to contribute

See [CONTRIBUTING](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/-/blob/master/CONTRIBUTING.md)
