## [6.1.3](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/compare/v6.1.2...v6.1.3) (2022-03-14)

## [6.1.2](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/compare/v6.1.1...v6.1.2) (2022-01-10)

## [6.1.1](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/compare/v6.1.0...v6.1.1) (2022-01-10)

# [6.1.0](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/compare/v6.0.0...v6.1.0) (2021-11-08)


### Features

* better Typescript support ([85ae0fe](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/85ae0fe73e139df334559e73f029e1410621d2c0))

# [6.0.0](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/compare/v5.0.1...v6.0.0) (2021-02-24)


### Build System

* drop support for CRA v3.x ([30ff21c](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/30ff21c9801430cb22c71106fc46e4f0d4db94c5))
* upgrade @commonground/eslint-config v4.x -> v5.x ([2a5c819](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/2a5c81966412fb5bb0d180b5fdcb71df59c389b0)), closes [/gitlab.com/commonground/core/eslint-config/-/blob/master/CHANGELOG.md#500-2021-02-24](https://gitlab.com//gitlab.com/commonground/core/eslint-config/-/blob/master/CHANGELOG.md/issues/500-2021-02-24)


### BREAKING CHANGES

* See https://github.com/facebook/create-react-app/releases/tag/v4.0.0
*

## [5.0.1](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/compare/v5.0.0...v5.0.1) (2021-01-06)


### Bug Fixes

* version mismatch ([399fff2](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/399fff25e7f4f6f8a0bebb5de2b2fd03ea47fe48))

# [5.0.0](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/compare/v4.0.0...v5.0.0) (2020-11-03)


### Build System

* upgrade eslint-config-standard from v14.x -> v16.x ([e9cf38a](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/e9cf38af57e6b7186d35e8ab18aed1428e8af8d9)), closes [/github.com/standard/standard/blob/master/CHANGELOG.md#1500---2020-10-21](https://gitlab.com//github.com/standard/standard/blob/master/CHANGELOG.md/issues/1500---2020-10-21) [/github.com/standard/standard/blob/master/CHANGELOG.md#1600---2020-10-28](https://gitlab.com//github.com/standard/standard/blob/master/CHANGELOG.md/issues/1600---2020-10-28)
* upgrade eslint-config-standard-react to v11.x ([7b33a15](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/7b33a15b11f3554e89d6bbbab019b2654015d2f1))


### Features

* add support for react-scripts v4.x ([c89c619](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/c89c6199a56c838e8df4e0fa3fa62d033c0622d1))


### BREAKING CHANGES

* eslint-config-standard-jsx is not included anymore,
so you will have to install it as an extra dependency.
* Updated rules for v15:

# [4.0.0](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/compare/v3.0.0...v4.0.0) (2020-10-01)


### Build System

* upgrade @commonground/eslint-config-cra-standard-prettier to v4.x ([c13bc1f](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/c13bc1fea276dca5b49d85eef07b3a5841ef7d0d))


### BREAKING CHANGES

* when a security issue occurs,
the linting output will result in errors

# [3.0.0](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/compare/v2.1.1...v3.0.0) (2020-09-14)


### Build System

* upgrade to ESLint v7.x and eslint-plugin-jest to v14.x ([637ddc8](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/637ddc821bc9024a192ca0baebf6e8f8b26980c1))


### BREAKING CHANGES

* ESLint changelog:
https://eslint.org/docs/user-guide/migrating-to-7.0.0

eslint-plugin-jest changelog:
https://github.com/jest-community/eslint-plugin-jest/releases/tag/v24.0.0

## [2.1.1](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/compare/v2.1.0...v2.1.1) (2020-09-07)


### Bug Fixes

* downgrade flowtype ([5dcfea3](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/5dcfea3b85742345be450425f152b893acaf5f9b))

# [2.1.0](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/compare/v2.0.0...v2.1.0) (2020-09-07)


### Features

* now reusing @commonground/eslint-config rules ([9b66815](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/9b668152d68099030f3d12d5512516b77df01650))

# [2.0.0](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/compare/v1.0.0...v2.0.0) (2020-04-06)


### Build System

* major version bump for peerDependencies ([399e3fc](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/399e3fc7403005d637f0fae72668dc2c327c4239))


### BREAKING CHANGES

* ```
eslint-plugin-node v10.x -> v11.x
prettier v1.x -> v2.x
```

Loosen support for CRA (accept v3.x range)

# 1.0.0 (2020-03-30)


### Features

* add license header checking ([8dcd925](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/8dcd92520426de5ccf9c1429474685ce8bc4368e))
* add license header checking ([954d5b5](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/954d5b5eb3c24c35f9c154f4a4c9376c92051c4b))
* Get package ready for publishing ([2519cee](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/2519cee520f2dc70f5dca9b8bc850c15328b491d))
* Initial setup ([3440c9b](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/3440c9b967704f664f60f96ba8ce3b7b6e65a590))
* reconfigure license header formatting to inline ([9368249](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/commit/9368249fcf012d7e005694bbb36612f076985f6b))


### BREAKING CHANGES

* from block to inline format

**Before:**

```
/**
* Copyright © VNG Realisatie 2020
* Licensed under the EUPL
*/
```

**After:**

```
// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
```
